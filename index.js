const body = document.body
const button = document.createElement('button', )

button.textContent = "Find By ID"
body.prepend(button)
const request = async (url) => {
  const response = await fetch(url)
  const userInfo = await response.json()
  
  return userInfo
}

button.addEventListener('click', async () => {
  const { ip } = await request('https://api.ipify.org/?format=json')
  const {timezone, country, regionName, city } = await request(`http://ip-api.com/json/${ip}`)
  const output = document.createElement('div')
 
  output.textContent = `${timezone.split('/')[0]}, ${country.toUpperCase()}, ${regionName}, ${city}`
  body.append(output)
})





